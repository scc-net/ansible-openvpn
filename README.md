# Ansible Role: OpenVPN

Installs and configures OpenVPN on Debian servers.

## Role Variables
    
    # OpenVPN server l3 instances (tun)
    openvpn_tun_instances:
      - name: vpn-udp-tun
        service_ip_address: "{{ service_address_1 }}"
        protocol: udp
        # routed ipv4 client subnets (one per node)
        tun_ipv4_addresses:
          - 203.0.113.0/26
          - 203.0.113.64/26
          - 203.0.113.128/26
          - 203.0.113.192/26
        # routed ipv6 client subnets (one per node)
        tun_ipv6_addresses:
          - 2001:db8:12:400::1/64
          - 2001:db8:12:401::1/64
          - 2001:db8:12:402::1/64
          - 2001:db8:12:403::1/64

    # OpenVPN server l2 instances (tap)
    openvpn_tap_instances:
      - name: vpn-udp-tap
        service_ip_address: "{{ service_address_2 }}"
        protocol: udp
    
    # VLAN interfaces for VPN2VLAN access (tap) in multiple VLANs
    vpn2vlan:
      # ip address of the first node
      - address: 192.0.2.1
        realms:
          - name: abc
            default_route: true
          - name: abc-split
            split_subnets_ipv4: "{{ split_subnets_ipv4 }}"
        radius:
          group: OE-VPN-abc 
        dhcp_config:
          domain:
          - abc.kit.edu
          domain_search:
          - foo.kit.edu
          - bar.kit.edu
          pool_range:
          - 192.0.2.22
          - 192.0.2.40
          resolvers: "{{ resolvers_vpn }}"
      - address: 192.0.3.65
        realms:
          - name: foo
            default_route: false
        radius:
          group: OE-VPN-foo
          two_factor: true
        dhcp_config:
          pool_range:
          - 192.0.3.100
          - 192.0.3.109
      - address: 192.0.11.129
        realms:
          - name: bar
            default_route: true
        radius:
          group: OE-VPN-bar
        dhcp_servers: "{{ dhcp_servers_xy }}"

